<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absents', function (Blueprint $table) 
        {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('nomor_induk');
            $table->string('absent');
            $table->text('description');
            $table->date('tanggal_cuti');

            $table->timestamps();
            // $table->foreign('employee_id')->references('id')->on('employees')
            //         ->onUpdate('CASCADE')
            //         ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absents');
    }
}
