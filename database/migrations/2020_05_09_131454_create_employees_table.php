<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('position_id');
            $table->string('name');
            $table->string('email');
            $table->string('password');
            $table->text('address');
            $table->date('tanggal_lahir');
            $table->string('gender');
            $table->string('nomor_induk');
            $table->date('start_work');
            $table->string('status');

            $table->timestamps();
            // $table->foreign('department_id')->references('id')->on('departments')
            //         ->onUpdate('CASCADE')
            //         ->onDelete('CASCADE');
            // $table->foreign('position_id')->references('id')->on('positions')
            //         ->onUpdate('CASCADE')
            //         ->onDelete('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
