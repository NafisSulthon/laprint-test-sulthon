<?php

Route::get('/', 'HomeController@index')->name('dashboard');

Route::get('/department/data', 'DataController@departments')->name('department.data');
Route::resource('/department', 'DepartmentController');

Route::get('/position/data', 'DataController@positions')->name('position.data');
Route::resource('position', 'PositionController');

Route::get('/category/data', 'DataController@categories')->name('category.data');
Route::resource('category', 'CategoryController');

Route::get('/employee/data', 'DataController@employees')->name('employee.data');
Route::resource('employee', 'EmployeeController');
Route::get('/employe/data/first','DataController@employeesFirst')->name('employee.data.first');
Route::get('/employe/data/first/view','EmployeeController@employeesFirstView')->name('employee.data.first.view');

Route::get('/absent/data', 'DataController@absents')->name('absent.data');
Route::resource('absent', 'AbsentController');

Route::get('/benefit/data', 'DataController@benefits')->name('benefit.data');
Route::resource('benefit', 'BenefitController');

Route::resource('laporan', 'LaporanController');