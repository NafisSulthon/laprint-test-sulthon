<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Absents extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function employee()
    {
        return $this->belongsTo(Employees::class);
    }
}
