<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Employees;
use App\Departments;
use App\Positions;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.employee.index', [
            'title' => 'Karyawan | Welcome'
        ]);
    }

    public function employeesFirstView()
    {
        return view('admin.employee.indexFirst', [
            'title' => 'Karyawan | Welcome'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.employee.create', [
            'title' => 'Tambah Data Karyawan | Welcome'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [        
            'name' => 'required|min:10',
            'alamat'=> 'required|min:10',
            'status' => 'required'
        ]);

        Employees::create([
            'department_id' => 0,
            'position_id' => 0,
            'name' => $request->name,
            'email' => '-',
            'password' =>0,
            'tanggal_lahir' => $request->tl,
            'status' => $request->status,
            'gender' => '-',
            'address' => $request->alamat,
            'nomor_induk' => $request->nomor_induk,
            'start_work' => $request->tb
        ]);

        return redirect()->route('admin.employee.index')->with('success', 'Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employees $employee)
    {
        return view('admin.employee.edit', [
            'title' => 'Update Karyawan | Welcome',
            'employee' => $employee,
            'departments' => Departments::orderBy('name', 'ASC')->get(),
            'positions' => Positions::orderBy('name', 'ASC')->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employees $employee)
    {
        $employee->update([
            
            'name' => $request->name,
            'address' => $request->address,
            'nomor_induk' => $request->nomor_induk
        ]);

        return redirect()->route('admin.employee.index')->with('info', 'Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employees $employee)
    {
        $employee->delete();

        return redirect()->route('admin.employee.index')->with('danger', 'Data Berhasil Dihapus');
    }
}
