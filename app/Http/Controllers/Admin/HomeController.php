<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Employees;
use App\Benefits;
use App\Departments;

class HomeController extends Controller
{
    public function index()
    {
        return view('admin.home', [
            'title' => 'Dashboard | Welcome',
            'employee' => Employees::count(),
            'benefit' => Benefits::sum('nominal'),
            'department' => Departments::count()
        ]);
    }
}
