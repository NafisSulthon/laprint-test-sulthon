<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Departments;
use App\Positions;
use App\Categories;
use App\Employees;
use App\Absents;
use App\Benefits;

class DataController extends Controller
{
    public function departments()
    {
        $department = Departments::orderBy('name', 'ASC');

        return datatables()->of($department)
                        ->addColumn('action', 'admin.department.action')
                        ->addIndexColumn()
                        // ->rawColumn(['action'])
                        ->toJson();
    }

    public function positions()
    {
        $position = Positions::orderBy('name', 'ASC');

        return datatables()->of($position)
                        ->addColumn('action', 'admin.position.action')
                        ->addIndexColumn()
                        // ->rawColumn(['action'])
                        ->toJson();
    }

    public function categories()
    {
        $categorie = Categories::orderBy('name', 'ASC');
        return datatables()->of($categorie)
                        ->addColumn('action', 'admin.category.action')
                        ->addIndexColumn()
                        // ->rawColumn(['action'])
                        ->toJson();
    }

    public function employees()
    {
        $employee = Employees::orderBy('name', 'ASC');
        return datatables()->of($employee)
                        ->addColumn('action', 'admin.employee.action')
                                        
                        ->addIndexColumn()
                        ->toJson();
    }

    public function employeesFirst()
    {
        $employee = Employees::orderBy('name', 'ASC')->take(3)->get();
        return datatables()->of($employee)
                        ->addColumn('action', 'admin.employee.action')
                                        
                        ->addIndexColumn()
                        ->toJson();
    }
    
    public function absents()
    {
        $absents = Absents::orderBy('absent', 'ASC');
        return datatables()->of($absents)
                        ->addColumn('sisa', function($absents){
                            $variable = '';
                           if ($absents->absent == 0 || $absents->absent == null) {
                            $variable = 'Utuh';
                           }else{
                             $variable = 12 - $absents->absent;
                           }
                            return $variable;
                        })                
                        ->addIndexColumn()
                        ->toJson();
    }
    
    public function benefits()
    {
        $benefit = Benefits::orderBy('nominal', 'ASC');
        return datatables()->of($benefit)
                        ->addColumn('action', 'admin.benefit.action')
                        ->addColumn('employee', function(Benefits $model){
                            return $model->employee->name;
                        })          
                        ->addColumn('categorie', function(Benefits $model){
                            return $model->categorie->name;
                        })
                        ->addIndexColumn()
                        ->toJson();
    }
}
