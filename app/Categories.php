<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function benefit()
    {
        return $this->HasMany(Benefits::class);
    }
}
