<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Benefits extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function employee()
    {
        return $this->belongsTo(Employees::class);
    }

    public function categorie()
    {
        return $this->belongsTo(Categories::class);
    }
}
