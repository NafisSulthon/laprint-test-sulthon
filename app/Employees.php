<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employees extends Model
{
    protected $guarded = [];

    public $timestamps = false;

    public function department()
    {
        return $this->belongsTo(Departments::class);
    }

    public function position()
    {
        return $this->belongsTo(Positions::class);
    }

    public function absents()
    {
        return $this->hasMany(Absents::class);
    }
}
