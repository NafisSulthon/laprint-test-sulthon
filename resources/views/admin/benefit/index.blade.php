@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Gaji & Tunjangan    
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Gaji & Tunjangan</li>
  </ol>
</section>
    
<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Gaji & Tunjangan</h3>
            
            <div style="float: right!important">
              <a href="{{ route('admin.benefit.create') }}" class="btn btn-primary btn-sm">+ Tambah Data Gaji & Tunjangan</a>
            </div>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>NO</th>
                <th>Nama Karyawan</th>
                <th>Kategori</th>
                <th>Nominal</th>                
                <th>Status</th>                
                <th>Aksi</th>
              </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

<form action="" method="post" id="deleteForm">
  @csrf
  @method("DELETE")
  <input type="submit" value="Hapus" style="display: none">
</form>

@endsection

@push('script')
    
    <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>
  
    @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.benefit.data') }}',
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'employee' },
              { data: 'categorie' },
              { data: 'nominal' },
              { data: 'status' },
              { data: 'action' }
            ]
          })
        })
    </script>

@endpush