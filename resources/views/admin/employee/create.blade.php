@extends('admin.template.default')

@section('content')

<section class="content-header">
    <h1>
      Karyawan <small>Tambah Data</small>   
    </h1>
    <ol class="breadcrumb">
    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('admin.department.index') }}"></i> Karyawan</a></li>
      <li class="active">Tambah Data Karyawan</li>
    </ol>
  </section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ route('admin.employee.store')}}" method="POST">
                @csrf
                <div class="box-body">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Nama Karyawan</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" placeholder="Masukan Nama Karyawan" value="{{ old('name') }}">
                            @error('name')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('alamat') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Alamat Karyawan</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="alamat" placeholder="Masukan Alamat Karyawan" value="{{ old('alamat') }}">
                            @error('alamat')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('alamat') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Nomor Induk</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nomor_induk" placeholder="Masukan Nomor Induk Karyawan">
                          
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Tanggal Lahir Karyawan</label>

                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="tl" >
                          
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('tb') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Tanggal Bergabung Karyawan</label>

                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="tb" >
                           
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('status') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Status</label>

                    <div class="col-sm-10">
                        <select name="status" class="form-control select2">
                            <option value="Aktif">Aktif</option>
                            <option value="Tidak aktif  ">Tidak Aktif</option>
                        </select>
                        @error('status')
                            <span class="help-block">{{ $message }}</span>
                        @enderror
                    </div>
                </div>
                
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                <button type="submit" class="btn btn-info">Tambah Data</button>
                </div>
                <!-- /.box-footer -->
            </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script>
    $( function() {
    $( "#date" ).datepicker();
  } );
</script>
@endsection