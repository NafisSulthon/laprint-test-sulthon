@extends('admin.template.default')

@section('content')

<section class="content-header">
    <h1>
      Karyawan <small>Update Data</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{ route('admin.employee.index') }}">Karyawan</a></li>
      <li class="active">Update Karyawan</li>
    </ol>
  </section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Update Data Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ route('admin.employee.update', $employee)}}" method="POST">
                @csrf
                @method("PUT")
                <div class="box-body">
                    <div class="form-group @error('name') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Nama Karyawan</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" placeholder="Masukan Nama Karyawan" value="{{ old('name') ?? $employee->name }}">
                            @error('name')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('address') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Alamat</label>

                        <div class="col-sm-10">
                            <textarea name="address" rows="3" class="form-control">{{ old('address') ?? $employee->address }}</textarea>
                        
                            @error('address')
                                <span class="help-block">{{ $message }}</span>
                            @enderror
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nomor Induk</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="nomor_induk" placeholder="Nomor Induk Karyawan" value="{{ old('nomor_induk') ?? $employee->nomor_induk }}">
                           
                        </div>
                    </div>
                </div>

                

               
                   
                <!-- /.box-body -->
                <div class="box-footer">
                <button type="submit" class="btn btn-info">Update Data</button>
                </div>
                <!-- /.box-footer -->
            </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
@endsection