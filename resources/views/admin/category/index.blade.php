@extends('admin.template.default')

@section('content')
    
<section class="content-header">
  <h1>
    Kategori    
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
  <li>Administrasi</a></li>
    <li class="active">Kategori</li>
  </ol>
</section>

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Kategori</h3>
            
            <div style="float: right!important">
              <a href="{{ route('admin.category.create') }}" class="btn btn-primary btn-sm">+ Tambah Data Kategori</a>
            </div>

          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>NO</th>
                <th>Nama</th>
                <th>Tipe</th>                
                <th>Deskripsi</th>                
                <th>Status</th>                
                <th>Aksi</th>
              </tr>
              </thead>

              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

<form action="" method="post" id="deleteForm">
  @csrf
  @method("DELETE")
  <input type="submit" value="Hapus" style="display: none">
</form>

@endsection

@push('script')
    
    <script src="{{ asset('admin/plugins/bs-notify.min.js') }}"></script>
  
    @include('admin.template.partials.alert')

    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.category.data') }}',
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'name' },
              { data: 'type' },
              { data: 'description' },
              { data: 'status' },
              { data: 'action' }
            ]
          })
        })
    </script>

@endpush