@extends('admin.template.default')

@section('content')

<section class="content-header">
  <h1>
    Absensi    
  </h1>
  <ol class="breadcrumb">
  <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Absensi</li>
  </ol>
</section>
    
<section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header">
            <h3 class="box-title">Data Absensi</h3>
            <div style="float: right!important">
              <a href="{{ route('admin.absent.create') }}" class="btn btn-primary btn-sm">+ Tambah Data Cuti Karyawan</a>
            </div>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table id="dataTable" class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>NO</th>
                <th>Nomor Induk</th>
                <th>Nama Karyawan</th>
                <th>Tanggal Cuti</th>
                <th>Sisa Cuti</th>               
                <th>Deskripsi</th>                
                {{-- <th>Aksi</th> --}}
              </tr>
              </thead>

              <tbody>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
</section>

@endsection

@push('script')
    
    <script>
        $(function() {
          $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.absent.data') }}',
            columns: [
              { data: 'DT_RowIndex', orderable: false, searchable: false },
              { data: 'nomor_induk' },
              { data: 'nama' },
              { data: 'tanggal_cuti' },
              { data: 'sisa' },
              { data: 'description' }
              // { data: 'action' }
            ]
          })
        })
    </script>

@endpush