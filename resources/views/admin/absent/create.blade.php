@extends('admin.template.default')

@section('content')

<section class="content-header">
    <h1>
      Karyawan <small>Tambah Data Absen </small>   
    </h1>
    <ol class="breadcrumb">
    <li><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="{{ route('admin.department.index') }}"></i> Karyawan</a></li>
      <li class="active">Tambah Data Absen Karyawan</li>
    </ol>
  </section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Tambah Data Absen Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="{{ route('admin.absent.store')}}" method="POST">
                @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="" class="col-sm-2 control-label">Nama Karyawan</label>

                        <div class="col-sm-10">
                          
                               <select name="name" id="" class="form-control">
                               @foreach ($employee_id as $data)
                                <option value="{{ $data->id }}">{{ $data->name }}</option>
                                @endforeach
                               </select>
                          
                           
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('alamat') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Lama Mengambil Cuti</label>

                        <div class="col-sm-10">
                            <input type="number" class="form-control" name="lama" placeholder="Masukan Lama Cuti">
                           
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('alamat') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Keterangan</label>

                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="keterangan" placeholder="Masukan Keterangan">
                           
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="form-group @error('tb') has-error @enderror">
                        <label for="" class="col-sm-2 control-label">Tanggal Absen</label>

                        <div class="col-sm-10">
                            <input type="date" class="form-control" name="ta" >
                           
                        </div>
                    </div>
                </div>

                
                <!-- /.box-body -->
                <div class="box-footer">
                <button type="submit" class="btn btn-info">Tambah Data Absen</button>
                </div>
                <!-- /.box-footer -->
            </form>
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script>
    $( function() {
    $( "#date" ).datepicker();
  } );
</script>
@endsection